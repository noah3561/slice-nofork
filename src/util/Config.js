/* eslint-disable max-len */
exports.messages = {
  prefix: 'si!'
};

exports.files = {
  commands: './commands',
  events: './events',
  logs: './logs'
};

exports.mdb = {
  blacklist: 'blacklist',
  donations: 'donations',
  guilds: 'guilds',
  afk: 'afk'
};

exports.lang = {
  musicDisabled: 'Hey! I see you want to use my music features! This is wonderful, but I\'d like to make one thing aware to you. ' +
    'My developer makes another bot that is completely dedicated to music! It\'s called FMRadio, and the invite can be found using the link below. ' +
    'Why do we suggest this over my music? Well, this bot\'s music features are extremely limited, and not nearly as customizeable. ' +
    'If you would still prefer to use my limited music features, you can disable them using %command.\n\n' +
    '<https://discordapp.com/oauth2/authorize?permissions=36768768&scope=bot&client_id=255933783293820928>'
};

exports.colors = {
  orange: '#FE8B00'
};

exports.owners = [
  '112732946774962176'
];